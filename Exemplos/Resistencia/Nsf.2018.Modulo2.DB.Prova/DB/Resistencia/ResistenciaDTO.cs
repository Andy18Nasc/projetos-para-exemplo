﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo2.DB.Prova.DB.Resistencia
{
    class ResistenciaDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Mensagem { get; set; }
    }
}
